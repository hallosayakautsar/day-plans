package app.ecommerce.ui.activity;


import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.KotaAdapter;
import app.ecommerce.ui.model.KotaModel;
import app.ecommerce.ui.utils.Tools;
import app.ecommerce.ui.utils.ViewAnimation;

public class ActivityDayPlans extends AppCompatActivity {

    private GoogleMap mMap;
    private NestedScrollView nested_scroll_view;
    private ImageButton  bt_toggle_detail;
    private View  lyt_expand_detail;

    private RecyclerView recyclerView,recyclerView2;
    private ArrayList<KotaModel> imageModelArrayList;
    private KotaAdapter adapter;

    private int[] myImageList = new int[]{R.drawable.gambar1, R.drawable.gambar2,R.drawable.gambar3, R.drawable.gambar4,R.drawable.gambar5,R.drawable.gambar6,R.drawable.gambar7};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        initToolbar();

        initComponent();

        initMapFragment();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.white));
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.colorPrimaryDark);
        Tools.setSystemBarLight(this);
    }

    private void initComponent() {




        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView2 = (RecyclerView) findViewById(R.id.recycler2);

        imageModelArrayList = KotaKu();
        adapter = new KotaAdapter(this, imageModelArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        recyclerView2.setAdapter(adapter);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));


        bt_toggle_detail = (ImageButton) findViewById(R.id.bt_toggle_detail);
        lyt_expand_detail = (View) findViewById(R.id.lyt_expand_detail);
        lyt_expand_detail.setVisibility(View.GONE);

        bt_toggle_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSectionDetail(bt_toggle_detail);
            }
        });




        nested_scroll_view = (NestedScrollView) findViewById(R.id.nested_scroll_view);
    }

    private void initMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = Tools.configActivityMaps(googleMap);
                MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(-6.9370126, 106.8822903));
                mMap.addMarker(markerOptions);
                mMap.moveCamera(zoomingLocation());
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        try {
                            mMap.animateCamera(zoomingLocation());
                        } catch (Exception e) {
                        }
                        return true;
                    }
                });
            }
        });
    }

    private CameraUpdate zoomingLocation() {
        return CameraUpdateFactory.newLatLngZoom(new LatLng(-6.9370126, 106.8822903), 13);
    }

    private ArrayList<KotaModel> KotaKu(){

        ArrayList<KotaModel> list = new ArrayList<>();

        for(int i = 0; i < 7; i++){
            KotaModel kota = new KotaModel();
            kota.setImage_drawable(myImageList[i]);
            list.add(kota);
        }

        return list;
    }

    private void toggleSectionDetail(View view) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.expand(lyt_expand_detail, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                    Tools.nestedScrollTo(nested_scroll_view, lyt_expand_detail);
                }
            });
        } else {
            ViewAnimation.collapse(lyt_expand_detail);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }
}
